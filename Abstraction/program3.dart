abstract class Parent{
	void fun(){
		print("In fun");
		gun();
	}
	void gun();
}
class Child extends Parent{
	void fun(){
		super.fun();
	}
	void gun(){	
		print("Child In gun");
	}
}
void main(){
	Child obj=new Child();
	obj.fun();
}

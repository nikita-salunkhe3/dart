abstract class Parent{
	int x;
	int y;
	Parent(this.x,this.y);

	void getData();
}
class Child extends Parent{
	Child(int x,int y):super(x,y);

	void getData(){
		print(x);
		print(y);
		super.getData();
	}
}
void main(){
	Child obj=new Child(10,29);
	obj.getData();
}

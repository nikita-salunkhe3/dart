class Company{
	final String? compname;
	final int? empCount;
	/*
	Company(){//Normal Constructor
		print("In Normal Constructor");
	}

	Company(this.compname,this.empCount){
		print("In parametrized Constructor");//Parameterized Constructor
	}
	
	//Named Constructor
	Company.named(this.compname,this.empCount){
		print("In named Constructor");
	}*/
	
	const Company(this.compname,this.empCount);

	void getInfo(){
		print("In getInfo");
		print(compname);
		print(empCount);
	}
}
void main(){

	print("In main");
//	Company();

	Company obj=const Company("Veritas",432000);
	obj.getInfo();
}	

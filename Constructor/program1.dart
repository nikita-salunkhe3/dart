class CricPlayer{
		
	String? name=null;
	int? jerNo=0;
	/*
	CricPlayer(this.name,this.jerNo){ // Normal Constructor
	
		print("Normal Constructor");
		print(name);
		print(jerNo);
	}
	CricPlayer(this.name,[this.jerNo=18]){///Optional Parameter=Change Chalto
		print("In Optional Parameter");
		print(name);
		print(jerNo);
	}
	CricPlayer(this.name,{this.jerNo=18}){///Default Parameter=Change nahi Chalto
		print("In Default Parameter");
		print(name);
		print(jerNo);
	}*/
	CricPlayer({this.name,this.jerNo}){
		print("In Named Constructor");
	}
}
	
void fun({int? x,int? y}){//for Named parameter function there is requiredment of Nullable Parameter
//because it consists of Nullable value bydefault sor we have to give ? or give the default value 
//error is related to this
	print("In named Function Parameter");
}
void gun(int x,int y)	{
	print("In gun Normal Function");
}
void run(int x,[int y=100]){
	print("In optional Paramter function");
	print(x);
	print(y);
}
void sun(int x,{int y=100}){
	print("In default Parameter function");
	print(x);
	print(y);
}
void main(){
		
/*
	CricPlayer obj=new CricPlayer("virat");
	
	CricPlayer obj1=CricPlayer("Rohit");

	new CricPlayer("MSD",);

	CricPlayer("SKY");*/

	CricPlayer(name:"Virat",jerNo:18);

	fun(x:10,y:20);
	gun(10,20);
	run(10,20);
	sun(10);
}
/*
output:
Normal Constructor
virat 
18
*/

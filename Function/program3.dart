void fun(int x){//Normal Function
	print("In fun");
}
void gun({int? x}){//Named Parameter  (x:10) pass Compulsory
	print("In gun");
	print(x);
}
void Gun({int? x=5}){//Named Parameter  (x:10) pass Compulsory
	print("In Gun");
	print(x);
}
void run({int x=20}){
	print("In run"); //Optional with defalut parameter
}
void man([int x=20]){
	print("In man");
	print(x); //Optional with Positional parameter
}
void Man([int? x]){
	print("In Man++++");//Optional with positional Parameter
	print(x);
}
void main(){
	print("In main");
	fun(10);
	gun(x:10);
	Gun(x:100);
	run();
	man(500);
	Man(1000);
}

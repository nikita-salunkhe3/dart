//print number of days in particular month
void main(){
  int month = 2;
  int year = 2004;
  if(month == 1){
    print("Jan has 31 days");
  }else if(month == 2){
    if(year %4 == 0){
      print("leep year- feb has 28 days");
    }else{
      print("feb has 27 days");
    }
  }else if(month == 3){
    print("March has 31 days");
  }else if(month == 4){
    print("April has 30 days");
  }else if(month == 5){
    print("May has 31 days");
  }else if(month == 6){
    print("June has 30 days");
  }else if(month == 7){
    print("July has 31 days");
  }else if(month == 8){
    print("August has 31 days");
  }else if(month == 9){
    print("Sept has 30 days");
  }else if(month == 10){
    print("Oct has 31 days");
  }else if(month == 11){
    print("Nov has 30 days");
  }else if(month == 12){
    print("Dec has 31 days");
  }else{
    print("Invalid month");
  }
}
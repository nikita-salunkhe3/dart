//check wether number is divisible by 3 and 5 as well as 3  or 5 or both not.
void main(){
  int num=10;
  if(num%3 == 0 && num%5 ==0){
    print("number is divisible by both 3 and 5");
  }else if(num%3 == 0){
    print("Number is divisible by 3");
  }else if(num%5 == 0){
    print("Number is divisible by 5");
  }else{
    print("Number is not divisible by 3 and 5");
  }
}
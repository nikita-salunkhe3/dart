/*
Write a program to count the digit of the given number
*/

import 'dart:io';
void main(){
	print("Enter the number");
	int num=int.parse(stdin.readLineSync()!);
	
	int count=0;
	while(num != 0){
		count++;
		num=num~/10;
	}
	print(count);
}

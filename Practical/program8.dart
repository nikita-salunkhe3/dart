/*
write a program to print the countdown of days to submit the assignment
*/

import 'dart:io';
void main(){
	print("Enter number");
	int num=int.parse(stdin.readLineSync()!);
	
	while(num >= 0){
		if(num == 0){
			print("0 days Assignment is overdue ");
		}else if(num == 1){
			print("1 day is remining");
		}else{
			print(num);
		}
		num--;
	}
}

/*
write a program to print the sum of all even numbers and the multiplication of odd number between 1 to 10
*/

void main(){
	int product=1;
	int sum=0;
	int i=1;
	while(i<=10){
		if(i%2==0){
			sum=sum+i;
		}else{
			product=product*i;
		}
		i++;
	}
	print("sum of even number between 1 to 10");
	print(sum);
	
	print("multiplication of odd number between 1 to 10");
	print(product);
}

/*
write a program to print the square of even digit if the given number
*/
import 'dart:io';
void main(){
	print("Enter the number");
	int num=int.parse(stdin.readLineSync()!);
	
	while(num!=0){
		int rem=num%10;
		if(rem%2==0)
			print(rem*rem);
		num=num~/10;
	}	
}
			

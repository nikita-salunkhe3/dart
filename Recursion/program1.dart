//print 1 to 10 using recursion code
void fun(int n){
	if(n==0){
		return;
	}
	fun(n-1);
	print(n);
}
void main(){
	fun(10);
}

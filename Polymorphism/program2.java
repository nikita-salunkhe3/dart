class Parent{
	int x=10;
	static int y=20;
}
class Child extends Parent{
	int x=100;
	static int y=200;

}
class Client{
	public static void main(String[] args){

		Parent obj=new Child();
		System.out.println(obj.x);
		System.out.println(obj.y);
	}
}

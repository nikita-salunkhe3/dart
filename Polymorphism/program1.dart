class Test{
	int? x;
	Test({int? x}){//named 
		print(this.x);
		this.x=x;
		print("In parent Constructor : $x");
	}

	void fun(){
		print("In parent fun : $x");
		this.x=99;
	}
}
class Test2 extends Test{
	int? x;
	Test2(this.x,int y):super(x:y){//x variable la Override keli 
		print("In child Constructor : $x");
	}

	void fun(){
		print("In child fun : $x");
		super.fun();
		print(x);
		print(super.x);
	}
}
void main(){
	Test2 obj=Test2(4,6);
	obj.fun();

	
}
/*

*/
	
		

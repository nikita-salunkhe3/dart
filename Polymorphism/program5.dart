class Parent{
	int x=10;
	void myData(int x){
		print(x);//3
		x=x;
	}
}
class Child extends Parent{
	int x=8;	
	void myData(int x){
		print(x); //3
		print(this.x);//8
		super.myData(x);
		super.x=this.x;// 10 = 8
		this.x=x;//8 = 3
		print(super.x);//8
	}
}
void main(){
	Child obj=new Child();
	obj.myData(3);
}
	

class Test{
	int? x;//6
	Test(this.x){
		print("Parent constructor: $x");
	}
	void fun(){
		print("fun x: $x");//4----represent child object
		print("fun this.x: $x");//4-----represent child object
		this.x=99;//99
	}
}
class Test2 extends Test{
	int? x;//4---6
	Test2(this.x,int y):super(y);
	
	void fun(){
		print(super.x);
		print(x);//4
		super.fun();//
		print(x);//99
		print(super.x);//6
	}
}
void main(){
	Test2 obj=new Test2(4,6);
	obj.fun();
}
/*
Parent constructor 4
6
4
fun x:4
fun this.x:4
99
6
*/

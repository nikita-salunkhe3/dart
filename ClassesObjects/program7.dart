class Client{
	final int? x;

	const Client(this.x);
}
void main(){
	Client obj1 = const Client(10);
	Client obj2 = Client(20);
	print(obj1.hashCode);
	print(obj2.hashCode);
	print(obj1.x);

//	obj1.x=obj2.x;
	obj1=obj2;
	print(obj1.hashCode);
	print(obj2.hashCode);
	print(obj1.x);
}

//To Access Private variable from the another file so that we can used gettersetter methods

class Client{
	String? _str;
	int? _x;
	double? _sal;
	int? _y;
	String? _name;
	
	Client(this._str,this._x,this._sal,this._y,this._name){
	
		print("In constructor");
	}
	//Type-1
	String? getStr(){
		return _str;
	}
	//Type-2
	int? get getX{
		return _x;
	}
	//Type-3
	get getSal{
		return _sal;	
	}
	//Type-4
	int? get getY => _y;

	//Type-5
	get getName => _name;	
}



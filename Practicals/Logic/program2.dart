/*
4   6   8   9   10
12  14  15  16  18
20  21  22  24  25 
26  27  28  30  32
33  34  35  36  38
*/

import 'dart:io';

void main(){
	print("Enter the row");
	int row=int.parse(stdin.readLineSync()!);
	
	int num=4;
	for(int i=1;i<=row;i++){
		for(int j=1;j<=row;){
			
			int count=0;
			for(int k=2;k<=num/2;k++){
				if(num%k == 0){
					count++;
				}
			}
			if(count>0){	
				stdout.write("$num  ");
				j++;
			}
			num++;				
		}
		print("");
	}
}


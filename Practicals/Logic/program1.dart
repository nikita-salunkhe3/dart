/*
1  2  3  4 
5  6  7  8 
9  10 11 12
13 14 15 16
*/

import 'dart:io';

void main(){
	print("Enter the row");
	int row=int.parse(stdin.readLineSync()!);

	int num=1;
	for(int i=1;i<=row*row;i++){
		if(i%row == 0){
			stdout.write("$num  ");
			print("");
		}else{
			stdout.write("$num  ");
		}
		num++;
	}
}
		

/*
1  2  3  4 
5  6  7  8 
9  10 11 12 
13 14 15 16
*/

import 'dart:io';
void main(){
	print("Enter the row");
	int row=int.parse(stdin.readLineSync()!);
	
	int num=1;
	int j=1;
	
	for(int i=1;i<=row;){
		if(j<=row){
			stdout.write("$num  ");
			num++;
			j++;
		}else{
			print("");
			j=1;
			i++;
		}
	}
}

/*
              2
          3   5
      7  11  13
   17 19 23  29
31 37 41 43  47
*/

import 'dart:io';

void main(){
	print("Enter the row");
	int row=int.parse(stdin.readLineSync()!);
	
	int num=1;
	
	for(int i=1;i<=row;i++){
		for(int sp=row;sp>i;sp--){
			stdout.write("	");
		}
		for(int j=1;j<=i;){
			int count=0;
			for(int k=2;k<=num/2;k++){
				if(num%k==0){
					count++;
				}
			}
			if(count==0 || num==2){
				stdout.write("$num	");
				j++;
			}
			num++;
		}
		print("");
	}
}

		
			


//              *
//           *  *  *
//        *  *  *  *  *
//     *  *  *  *  *  *  *
import 'dart:io';
void main(){
  int row=5;
  int num=0;

  for(int i=1;i<=row;i++){
    for(int sp=row;sp>i;sp--){
      stdout.write("   ");
    }
    for(int j=1;j<=i+num;j++){
      stdout.write("*  ");
    }
    num++;
    stdout.write("\n");
  }
}
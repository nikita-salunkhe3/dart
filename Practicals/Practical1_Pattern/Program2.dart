//                          *
//                    *  *  *
//              *  *  *  *  *
//        *  *  *  *  *  *  *

import 'dart:io';
void main(){
  int row = 4;
  int num=1;

  for(int i=1;i<=4;i++){
    int space = (row*2)-1;
    for(int sp=space; sp>num ;sp--){
      stdout.write("   ");
    }
    for(int j=1;j<=num;j++){
      stdout.write("*  ");
    }
    num=num+2;
    stdout.write("\n");
  }
}
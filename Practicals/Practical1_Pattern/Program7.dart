//           *
//        *  *
//     *  *  *
//        *  *
//           *

import 'dart:io';
void main(){
  int row=5;

  for(int i=1;i<=row;i++){
    if(i==(row/2)+1){
      for(int sp=(row~/2); sp>i;sp--){
        stdout.write("   ");
      }
      for(int j=1;j<=i;j++){
        stdout.write("*  ");
      }
    }else{
      for(int sp=1;sp<=i-(row~/2)-1;sp++){
        stdout.write("   ");
      }
      for(int j=i-(row~/2);j>=1;j++){
        stdout.write("*  ");
      }
    }
    stdout.write("\n");
  }

}
//* 
//*   *
//*   *   *
//*   *  
//*

import 'dart:io';
void main(){
  int row = 7;
  for(int i=1;i<=row;i++){
    if(i<=(row/2)+1){
      for(int j=1;j<=i;j++){
        stdout.write("*   ");
      }
    }else{
      for(int j=1;j<=row-i+1;j++){
        stdout.write("*   ");
      }
    }
    stdout.write("\n");
  }
}
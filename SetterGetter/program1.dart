class Demo{
	int? _x;
	String? _str;
	
	Demo(this._x,this._str);

	void disp(){
		print(_x);
		print(_str);
	}
}
void main(){
	Demo obj1=new Demo(10,"Kanha");
	
	obj1.disp(); // 10  Kanha
	
	obj1._x = 15;
	obj1._str="Rahul";
	
	obj1.disp(); // 15   Rahul
}

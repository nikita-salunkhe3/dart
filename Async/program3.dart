import 'dart:io';
Future<String?> getInput() async{
	
	print("Holt");
	Future.delayed(Duration(seconds:2));
	print("Enter your order");
	String? val=stdin.readLineSync();
	return val;
}
Future<String?> getOrder(){
	
	return Future.delayed(Duration(seconds:5),()=>getInput());
}
Future<String> getOrderMessage() async{
	
	var order = await getOrder();
	return "Your order is $order";
}
void main() async{
	
	print("Start main");	
	print(await getOrderMessage());
	print("End main");
}

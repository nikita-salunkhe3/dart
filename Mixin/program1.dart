mixin Demo1{
	void fun1(){
		print("In mixin fun1");
	}
	void gun();
}
class Demo with Demo1{
	void run(){
		print("In run");
	}
}
void main(){
	Demo obj=new Demo();
	obj.run();
	obj.fun1();
}

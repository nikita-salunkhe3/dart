mixin Demo{
	void data(){
		print("In Demo mixin");
	}
	void fun();
}
mixin Demo1 on Demo{
	void data(){
		print("In Demo1 mixin");
	}
	void fun(){
		print("In fun");
	}
}
class Child with Demo1{
	void data(){
		print("In child data");
	}
}
void main(){
	Child obj=new Child();
	obj.mydata();	
	obj.fun();
}


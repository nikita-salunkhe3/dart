mixin nikita{
	void data(){
		print("In Demo1");
	}
}
mixin sarthak on nikita{
	void data(){
		print("In Demo2");
	}
}
class Child on sarthak{
	void data(){
		print("In Sonya");
	}
	
}
void main(){
	
	Child obj=new Child();
	obj.data();
}
/*
program4.dart:11:7: Error: 'Object' doesn't implement 'Demo1' so it can't be used with 'Demo2'.
 - 'Object' is from 'dart:core'.
 - 'Demo1' is from 'program4.dart'.
 - 'Demo2' is from 'program4.dart'.
class Child with Demo2{
      ^
*/

class Parent{
	Parent(){
		System.out.println("In Parent Constructor");
	}
}
class Child extends Parent{
	Child(){
		Parent();
		System.out.println("In Child Constructor");
	}
}
class Client{
	public static void main(String[] args){

	Child obj=new Child();
	}
}	
		

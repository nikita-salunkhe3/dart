class Parent{
	Parent(){
		print("In Parent constructor");
	}
	call(){
		print("In Parent call method");
	}
}
class Child extends Parent{
	Child(){
		print("In child constructor");
	}
	call(){
		print("In Child Call Method");
	}
}
void main(){
	Child obj=new Child();
	obj();
}

class Parent{
	int x=10;
	String str="nikita";

	Parent(){
		print("In Parent Construuctor");
		print("Parent Consx: $x");
		print("Parent Consstr1: $str");
		print("Parent this : $this");
	}
	void ParentMethod(){
		print("In Parent method");	
		print("Parent M x: $x");
		print("Parent M str1: $str");
	}
}
class Child extends Parent{
	int y=20;
	String str1="sarthak";
	
	Child(){
		print("super:$super.x");
		print("Super :$super.str");
		print("In Child constructor");
		print("Child cons y: $x");
		print("Child cons str2: $str");
		print("child this : $this");
	}
	void ChildMethod(){
		print("In Child Method");	
		print("Child M y: $x");
		print("Child M str2: $str");
	}
}
void main(){	
	Child obj=new Child();

//	obj.ParentMethod();
//	obj.ChildMethod();
}



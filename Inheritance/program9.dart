class Parent{
	int? x;
	String? str;

	Parent(this.x,this.str){
		print("In Parent Constructor");
		print(x);
		print(str);
	}
}
class Child extends Parent{
	int? y;
	String? str2;

	Child(this.y,this.str2,int x,String str):super(x,str){
		print("In Child constructor");
		print(y);
		print(str2);
	}
}
void main(){
	Child obj=new Child(10,"nikita",20,"sarthak");
}


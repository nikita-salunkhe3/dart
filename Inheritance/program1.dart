class Parent{
	int x=10;
	String str1="name";
	void parentMethod(){
		print(x);
		print(str1);
	}
}
class Child extends Parent{
	
	int y=20;
	String str2="data";
}
void main(){
	print("In main");
	Child obj=new Child();
	obj.parentMethod();
}

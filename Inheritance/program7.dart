class Parent{
	int x=10;
	String str="nikita";

	Parent(){
		print("In Parent Contructor");
	}
	void parentMethod(){
		print("In Parent Method");
		print(this.x);	
		print(this.str);
	}
}
class Child extends Parent{
	int x=20;
	String str="sarthak";
	
	Child(){
		print("In Child Constructor");
	//error	print("Super hashcode: $super.hashCode");
	}
	void childMethod(){
		print("In Child method");
		print(x);
		print(str);
		print(super.x);
		print(super.str);
	}
}
void main(){
	Parent obj2=new Parent();
	obj2.parentMethod();

	Child obj=new Child();
	obj.parentMethod();
	obj.childMethod();

	Parent obj1=new Parent();
	obj.parentMethod();
}
/*
Conclusion:
Child class actual madhe Parent madhil sudha instance variable chi nav change kertoy
so 
*/
	
	

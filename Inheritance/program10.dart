class Parent{
	Parent(){
		print("In Parent constructor");
	}
	call(){
		print("In Parent call method");
	}
}
class Child extends Parent{
	Child(){
		print("In Child Constructor");
	}
	call(){
		print("In Child call method");
	}
}
void main(){
	Child obj=new Child();
}

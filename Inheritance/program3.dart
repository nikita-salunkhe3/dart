class Parent{
	String str="name";
	int x=10;

	Parent(){
		print("In Parent Constructor");
	}
	
	void parentMethod(){
		print("In Parent Method");
	}
}
class Child extends Parent{
	int y=20;
	String str2="data";

	Child(){
		print("In Child constructor");
	}
	
	void childMethod(){
		print("In child method");
	}
}
void main(){
	Parent obj=new Parent();
	obj.parentMethod();
	
	print(obj.str);
	print(obj.x);
	
//	print(obj.str2);//error
//	print(obj.y);//error
}
	

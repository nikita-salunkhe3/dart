//In dart we can't directly use the super() keyword that can be used in java 
// for super() it can be call to the parent constructor 
//for that we have to write call() method for it to call parent constructor

class Parent{
	int x=10;//if the parent and child contains same variable then Parent variable is hide 
	//and if we have to change the variable we used this so it will change the x variable
//value 
//so that for parent variable also it will gives child x variable

	Parent(){
		print("In Parent cons");
	}
	int call(){//main use of call method is that to make the object as a callable
//call() method implicitly call hote
		print(x);
		print("In call method");
		return 1;
	}
}
class Child extends Parent{
	int x=20;
	Child(){
		Parent();//Parent constructor  --->> Java Madhe ass chalat nahi......Cannot fnd symbol chi error yete
		this();
		super();
		print("Child x: $x");
		print("In child Cons");
	}
}
void main(){
	Child obj=new Child();
}
	
